package ua.softserve.academy.kv030.FX.api;

import ua.softserve.academy.kv030.FX.model.ResourceDTO;
import ua.softserve.academy.kv030.FX.model.ShareDataDTO;

import java.io.File;
import java.time.LocalDate;
import java.util.List;

public interface FileBoxService {

    void uploadFile(File file, LocalDate localDate);

    void downloadFile(String link);

    boolean deleteFile(long userId, long fileId);

    void shareFile(ShareDataDTO shareDataDTO);

    List<ResourceDTO> getUserFilesList(long userId);

}

