package ua.softserve.academy.kv030.FX.controllers;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import ua.softserve.academy.kv030.FX.api.FileBoxService;
import ua.softserve.academy.kv030.FX.api.FileBoxServiceImpl;
import ua.softserve.academy.kv030.FX.api.UserService;
import ua.softserve.academy.kv030.FX.api.UserServiceImpl;
import ua.softserve.academy.kv030.FX.model.PermissionDTO;
import ua.softserve.academy.kv030.FX.model.ResourceDTO;
import ua.softserve.academy.kv030.FX.model.ShareDataDTO;
import ua.softserve.academy.kv030.FX.model.UserDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShareFileController {
    @FXML
    RadioButton allUsersRadio, listOfUsersRadio;
    @FXML
    ListView<UserDTO> listOfUsers;
    @FXML
    TextField userSearchField;
    @FXML
    Label fileName;
    @FXML
    Button shareButton;

    private List<UserDTO> users;
    private BooleanProperty observable;
    private List<Long> usersId = new ArrayList<>();
    private ResourceDTO resourceDTO;

    public void setResourceDTO(ResourceDTO resourceDTO) {
        this.resourceDTO = resourceDTO;
    }

    public void initialize() {
        UserService userService = new UserServiceImpl();
        users = userService.getUsers();
        listOfUsers.setItems(FXCollections.observableList(users));
    }

    public void setFileName(String name) {
        fileName.setText(name);
    }

    public void shareFile() {
        FileBoxService fileBoxService = new FileBoxServiceImpl();
        ShareDataDTO shareDataDTO = new ShareDataDTO();
        shareDataDTO.setFileMetadata(resourceDTO);
        shareDataDTO.setPermittedUserIds(usersId);
        fileBoxService.shareFile(shareDataDTO);
    }

    public void selectAll() {
        usersId.clear();
        listOfUsers.setCellFactory(CheckBoxListCell.forListView(this::getAll));
        usersId.addAll(users.stream()
                .map(UserDTO::getId)
                .collect(Collectors.toList()));
        resourceDTO.setPermission(PermissionDTO.ALL_USERS);
    }

    public void selectSome() {
        usersId.clear();
        listOfUsers.setCellFactory(CheckBoxListCell.forListView(this::getSome));
        resourceDTO.setPermission(PermissionDTO.LIST_OF_USERS);
    }

    private ObservableValue<Boolean> getAll(UserDTO userDTO) {
        observable = new SimpleBooleanProperty();
        observable.setValue(true);
        return observable;
    }

    private ObservableValue<Boolean> getSome(UserDTO userDTO) {
        observable = new SimpleBooleanProperty();
        observable.setValue(false);
        observable.addListener((obs, wasSelected, isNowSelected) -> {
            if (obs.getValue() == true)
                usersId.add(userDTO.getId());
            else
                usersId.remove(userDTO.getId());
        });
        return observable;
    }
}
