package ua.softserve.academy.kv030.FX.api.retrofit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static ua.softserve.academy.kv030.FX.values.Constants.TOKEN_HEADER;

public class ServiceGenerator {

    private static Retrofit.Builder builder = new Retrofit.Builder()
            .addConverterFactory(JacksonConverterFactory.create());

    public static <T> T createService(Class<T> serviceClass, String baseURL) {
        return createService(serviceClass, "", baseURL);
    }

    public static <T> T createService(Class<T> serviceClass, String authToken, String baseURL) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (!authToken.equals("")) {
            httpClient.addInterceptor(chain -> {
                Request request = chain.request();
                Request.Builder newRequest = request.newBuilder().addHeader(TOKEN_HEADER, authToken);
                return chain.proceed(newRequest.build());
            });
        }

        httpClient.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        builder.baseUrl(baseURL).client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(serviceClass);
    }
}
