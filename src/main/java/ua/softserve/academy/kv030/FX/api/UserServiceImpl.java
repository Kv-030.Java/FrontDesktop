package ua.softserve.academy.kv030.FX.api;

import retrofit2.Call;
import retrofit2.Response;
import ua.softserve.academy.kv030.FX.api.retrofit.ServiceGenerator;
import ua.softserve.academy.kv030.FX.api.retrofit.UserServiceApi;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;
import ua.softserve.academy.kv030.FX.model.UserCredentialsDTO;
import ua.softserve.academy.kv030.FX.model.UserDTO;
import ua.softserve.academy.kv030.FX.values.Constants;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static ua.softserve.academy.kv030.FX.helpers.UITools.showNotif;
import static ua.softserve.academy.kv030.FX.values.Constants.AUTH_URL;
import static ua.softserve.academy.kv030.FX.values.Constants.USER_TOKEN;

public class UserServiceImpl implements UserService {

    private UserServiceApi userServiceApi;

    public UserServiceImpl() {
        this.userServiceApi = ServiceGenerator.createService(UserServiceApi.class, USER_TOKEN, AUTH_URL);
    }

    @Override
    public List<UserDTO> getUsers() {
        Call<List<UserDTO>> call = userServiceApi.getUsers();
        List<UserDTO> users = Collections.emptyList();
        try {
            Response<List<UserDTO>> response = call.execute();
            if (response.isSuccessful()){
                users = response.body();
                return users;
            }
            return users;
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
    }

    @Override
    public boolean login (UserCredentialsDTO userCredentialsDTO){
    Call<UserDTO> call = userServiceApi.login(userCredentialsDTO);

        try {
            Response<UserDTO> response = call.execute();
            if (call.isExecuted()) {
                showNotif("Successfully logged in");
                Constants.initCredentials(response);
                return true;
            } else {
                showNotif("Incorrect credentials");
                return false;
            }
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
}}
