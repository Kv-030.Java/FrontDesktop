package ua.softserve.academy.kv030.FX.helpers;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import ua.softserve.academy.kv030.FX.Main;
import ua.softserve.academy.kv030.FX.controllers.NotificationController;
import ua.softserve.academy.kv030.FX.controllers.NotificationWithLinkController;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;

import java.io.IOException;

public class UITools {

    public static void setNewPane(Pane centerPane, FXMLLoader fxmlLoader) {
        try {
            centerPane.getChildren().clear();
            centerPane.getChildren().add(fxmlLoader.load());
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
    }

    public static void showNotif(String head){
        Platform.runLater( () -> new UITools().showNotification(head));
    }

    public static void showNotifLink(String head, String link){
        Platform.runLater(() -> new UITools().showNotificationWithLink(head, link));
    }

    public static void showError(String error){
        Platform.runLater(() -> {
            new UITools().showNotification(error);
            throw new FrontDesktopException(error);
        });
    }

    public FXMLLoader loadShareFile(){
        return new FXMLLoader(getClass().getResource("/fxml/ShareFile.fxml"));
    }

    public void prepareStage(Parent root) {
        Stage littleStage = new Stage();
        littleStage.setTitle("Document Box");
        littleStage.getIcons().add(new Image(Main.class.getResourceAsStream("/images/image.jpg")));
        littleStage.setScene(new Scene(root));
        littleStage.showAndWait();
    }


    private void showNotification(String head) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Notification.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        NotificationController controller = loader.getController();
        controller.setMessage(head);
        prepareStage(root);
    }

    private void showNotificationWithLink(String head, String link) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/NotificationWithLink.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        NotificationWithLinkController controller = loader.getController();
        controller.setMessageAndLink(head, link);
        prepareStage(root);
    }

}
