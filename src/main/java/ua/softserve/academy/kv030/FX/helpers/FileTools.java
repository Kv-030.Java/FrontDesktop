package ua.softserve.academy.kv030.FX.helpers;

import okhttp3.ResponseBody;
import org.apache.commons.io.FileUtils;
import retrofit2.Response;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class FileTools {

    public static File encryptFile(File file, String key) {
        try {
            byte[] encryptedBytes = CipherService.encrypt(Files.readAllBytes(file.toPath()), key);
            return createTempFile(encryptedBytes, file.getName());
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
    }

    public static byte[] decryptBytes(byte[] bytes, String key) {
        return CipherService.decrypt(bytes, key);
    }

    public static File createTempFile(byte[] bytes, String fileName) {

        try {
            String prefix = fileName.substring(0, fileName.lastIndexOf("."));
            String suffix = fileName.substring(fileName.lastIndexOf("."));
            File tempFile = File.createTempFile(prefix, suffix);

            FileUtils.writeByteArrayToFile(tempFile, bytes);
            tempFile.deleteOnExit();
            return tempFile;
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
    }

    public static File createFile(byte[] bytes, String fileName) {
        File file = new File(System.getProperty("user.home") + "/" + fileName);
        try {
            FileUtils.writeByteArrayToFile(file, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static void openFile(File file) {
        try {
            Desktop.getDesktop().open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String fileNameFromResponse(Response<ResponseBody> response) {
        String header = response.headers().get("Content-Disposition");

        if (header != null)
            return header.replace("attachment; filename=", "");
        else
            return "unknownfile";
    }

    public static String fileKeyFromResponse(Response<ResponseBody> response) {
        String header = response.headers().get("Content-Key");

        if (header != null)
            return header;
        else
            return "mysupersecretkey";
    }
}
