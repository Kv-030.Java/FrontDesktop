package ua.softserve.academy.kv030.FX.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum PermissionDTO {
    ALL_USERS("ALL_USERS"),

    LIST_OF_USERS("LIST_OF_USERS");

    private String value;

    PermissionDTO(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
