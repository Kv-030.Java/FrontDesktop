package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import ua.softserve.academy.kv030.FX.api.FileBoxServiceImpl;

public class DownloadFileController {

    @FXML
    TextField fileLinkTextField;
    @FXML
    Button downloadFileButton;

    public void downloadFile() {
        FileBoxServiceImpl fileBoxApi = new FileBoxServiceImpl();
        fileBoxApi.downloadFile(fileLinkTextField.getText());
    }
}
