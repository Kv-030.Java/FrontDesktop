package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;

import java.io.IOException;

import static ua.softserve.academy.kv030.FX.values.Constants.USER_TOKEN;

public class MainViewController {

    @FXML
    BorderPane mainViewPane;
    @FXML
    Button uploadButton, listOfFilesButton, downloadButton, accountButton, supportButton;
    @FXML
    Pane centerPane;

    public void handleEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            Button button = (Button) event.getSource();
            switch (button.getText()) {
                case "UPLOAD FILE":
                    openUploadFile();
                    break;
                case "LIST OF FILES":
                    openListOfFiles();
                    break;
                case "DOWNLOAD FILE":
                    openDownloadFile();
                    break;
                case "ACCOUNT":
                    openAccount();
                    break;
                case "SUPPORT":
                    openSupport();
                    break;
            }
        }
    }

    public void openUploadFile() {
        openNewPane("/fxml/UploadFile.fxml");
    }

    public void openListOfFiles() {
        if (USER_TOKEN.equals("")) {
            openNewPane("/fxml/ListOfFiles.fxml");
        } else {
            FXMLLoader fxmlLoader = openNewPane("/fxml/ListOfFiles.fxml");
            ListOfFilesController controller = fxmlLoader.getController();
            controller.init();
        }
    }

    public void openDownloadFile() {
        openNewPane("/fxml/DownloadFile.fxml");
    }

    public void openAccount() {
        if (USER_TOKEN.equals("")) {
            openNewPane("/fxml/Account.fxml");
        } else {
            openNewPane("/fxml/Logged.fxml");
        }
    }

    public void openSupport() {
        openNewPane("/fxml/Support.fxml");
    }

    private FXMLLoader openNewPane(String paneName) {
        try {
            centerPane.getChildren().clear();
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(paneName));
            centerPane.getChildren().add(fxmlLoader.load());
            return fxmlLoader;
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
    }

}
