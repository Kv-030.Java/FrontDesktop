package ua.softserve.academy.kv030.FX.values;

import retrofit2.Response;
import ua.softserve.academy.kv030.FX.model.UserDTO;

public class Constants {

    public static final String TOKEN_HEADER = "X-AUTH";
    public static final String AUTH_URL = "http://localhost:8585/";
    public static final int KEY_SIZE = 16;
    public static final String CIPHER_ALGORITHM = "AES/CTR/NoPadding";
    public static final String KEY_ALGORITHM = "AES";
    public static final long CIPHER_FILE_SIZE = 52428800;

    public static String USER_TOKEN = "";
    public static String USER_EMAIL = "";
    public static Long USER_ID = 0L;
    public static String USER_ROLE = "";

    public static void cleanCredentials(){
        USER_TOKEN = "";
        USER_EMAIL = "";
        USER_ROLE = "";
        USER_ID = 0L;

    }

    public static void initCredentials(Response<UserDTO> response) {
        USER_TOKEN = response.headers().get(TOKEN_HEADER);
        USER_EMAIL = response.body().getEmail();
        USER_ROLE = response.body().getRole().name();
        USER_ID = response.body().getId();
    }

}
