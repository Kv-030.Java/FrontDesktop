package ua.softserve.academy.kv030.FX.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import ua.softserve.academy.kv030.FX.api.UserService;
import ua.softserve.academy.kv030.FX.api.UserServiceImpl;
import ua.softserve.academy.kv030.FX.model.UserCredentialsDTO;

import java.io.IOException;

import static ua.softserve.academy.kv030.FX.helpers.UITools.setNewPane;

public class AccountController {
    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField passwordTextField;
    @FXML
    Button loginButton;
    @FXML
    AnchorPane pane;

    public void login() throws IOException {
        UserCredentialsDTO userCredentialsDTO = new UserCredentialsDTO();
        userCredentialsDTO.setEmail(usernameTextField.getText());
        userCredentialsDTO.setPassword(passwordTextField.getText());

        UserService userService = new UserServiceImpl();
        boolean success = userService.login(userCredentialsDTO);
        if (success) Platform.runLater(this::openLoggedInView);
    }

    private void openLoggedInView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Logged.fxml"));
        setNewPane(pane, fxmlLoader);
    }
}
