package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;


public class NotificationWithLinkController {
    @FXML
    Label headLabel;
    @FXML
    TextField linkField;
    @FXML
    AnchorPane pane;

    public void setMessageAndLink(String head, String link) {
        headLabel.setText(head);
        linkField.setText(link);
    }


}
