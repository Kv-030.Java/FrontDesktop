package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class NotificationController {
    @FXML
    Label headLabel;
    @FXML
    Button closeButton;
    @FXML
    AnchorPane pane;

    public void setMessage(String head) {
        headLabel.setText(head);
        closeButton.requestFocus();
    }

    public void close() {
        Stage stage = (Stage) pane.getScene().getWindow();
        stage.close();
    }

    public void handleEnter(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            close();
        }
    }
}
