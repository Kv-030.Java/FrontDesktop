package ua.softserve.academy.kv030.FX.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ShareDataDTO   {
    @JsonProperty("fileMetadata")
    private ResourceDTO fileMetadata = null;

    @JsonProperty("permittedUserIds")
    private List<Long> permittedUserIds = null;

    public ShareDataDTO fileMetadata(ResourceDTO fileMetadata) {
        this.fileMetadata = fileMetadata;
        return this;
    }

    public ResourceDTO getFileMetadata() {
        return fileMetadata;
    }

    public void setFileMetadata(ResourceDTO fileMetadata) {
        this.fileMetadata = fileMetadata;
    }

    public ShareDataDTO permittedUserIds(List<Long> permittedUserIds) {
        this.permittedUserIds = permittedUserIds;
        return this;
    }

    public ShareDataDTO addPermittedUserIdsItem(Long permittedUserIdsItem) {
        if (this.permittedUserIds == null) {
            this.permittedUserIds = new ArrayList<Long>();
        }
        this.permittedUserIds.add(permittedUserIdsItem);
        return this;
    }

    public List<Long> getPermittedUserIds() {
        return permittedUserIds;
    }

    public void setPermittedUserIds(List<Long> permittedUserIds) {
        this.permittedUserIds = permittedUserIds;
    }


    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShareDataDTO shareData = (ShareDataDTO) o;
        return Objects.equals(this.fileMetadata, shareData.fileMetadata) &&
                Objects.equals(this.permittedUserIds, shareData.permittedUserIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileMetadata, permittedUserIds);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ShareDataDTO {\n");

        sb.append("    fileMetadata: ").append(toIndentedString(fileMetadata)).append("\n");
        sb.append("    permittedUserIds: ").append(toIndentedString(permittedUserIds)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

