package ua.softserve.academy.kv030.FX.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import ua.softserve.academy.kv030.FX.api.FileBoxService;
import ua.softserve.academy.kv030.FX.api.FileBoxServiceImpl;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;
import ua.softserve.academy.kv030.FX.helpers.UITools;
import ua.softserve.academy.kv030.FX.model.ResourceDTO;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static ua.softserve.academy.kv030.FX.values.Constants.USER_ID;

public class ListOfFilesController {

    @FXML
    ListView<ResourceDTO> listOfFiles;
    @FXML
    MenuItem downloadItem, deleteItem, shareItem;
    private FileBoxService fileBoxService = new FileBoxServiceImpl();

    public void init() {
        List<ResourceDTO> resources = fileBoxService.getUserFilesList(USER_ID);
        ObservableList<ResourceDTO> observableList;
        if (resources != null) {
            observableList = FXCollections.observableList(resources);
        } else {
            observableList = FXCollections.observableList(Collections.emptyList());
        }
        Platform.runLater(() -> listOfFiles.setItems(observableList));
    }

    public void downloadFile() {
        ResourceDTO resourceDTO = getSelectedItem();
        fileBoxService.downloadFile(resourceDTO.getFileUUID());
    }

    public void shareFile() {
        ResourceDTO resourceDTO = getSelectedItem();
        UITools uiTools = new UITools();
        FXMLLoader loader = uiTools.loadShareFile();
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }
        ShareFileController controller = loader.getController();
        controller.setFileName(resourceDTO.getFileName());
        controller.setResourceDTO(getSelectedItem());
        uiTools.prepareStage(root);
    }

    public void deleteFile() {
        ResourceDTO resourceDTO = getSelectedItem();
        if (fileBoxService.deleteFile(resourceDTO.getOwnerId(), resourceDTO.getResourceId()))
            listOfFiles.getItems().remove(resourceDTO);
    }

    private ResourceDTO getSelectedItem() {
        return listOfFiles.getSelectionModel().getSelectedItem();
    }
}
