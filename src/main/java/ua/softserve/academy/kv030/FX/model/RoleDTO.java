package ua.softserve.academy.kv030.FX.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum RoleDTO {

    ADMIN("ADMIN"),
    TECH_SUPP("TECH_SUPP"),
    USER("USER");

    private String value;

    RoleDTO(String value) {
        this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
        return String.valueOf(value);
    }

    @JsonCreator
    public static RoleDTO fromValue(String text) {
        for (RoleDTO b : RoleDTO.values()) {
            if (String.valueOf(b.value).equals(text)) {
                return b;
            }
        }
        return null;
    }
}


