package ua.softserve.academy.kv030.FX;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainView.fxml"));
        primaryStage.setTitle("Document Box");
        primaryStage.getIcons().add( new Image( Main.class.getResourceAsStream("/images/image.jpg")));
        primaryStage.setScene(new Scene(root, 700, 550));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
