package ua.softserve.academy.kv030.FX.api;

import ua.softserve.academy.kv030.FX.model.UserCredentialsDTO;
import ua.softserve.academy.kv030.FX.model.UserDTO;

import java.util.List;

public interface UserService {

    List<UserDTO> getUsers();

    boolean login (UserCredentialsDTO userCredentialsDTO);
}
