package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import ua.softserve.academy.kv030.FX.helpers.UITools;

import static ua.softserve.academy.kv030.FX.values.Constants.*;

public class LoggedController {
    @FXML
    Label emailLabel, idLabel, roleLabel;
    @FXML
    Button logoutButton;
    @FXML
    AnchorPane pane;

    public void initialize(){
        emailLabel.setText(USER_EMAIL);
        idLabel.setText(USER_ID.toString());
        roleLabel.setText(USER_ROLE);
    }

    public void logout(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Account.fxml"));
        UITools.setNewPane(pane, fxmlLoader);
        cleanCredentials();
    }
}
