package ua.softserve.academy.kv030.FX.exceptions;

import java.util.Arrays;

public class FrontDesktopException extends RuntimeException {


    public FrontDesktopException() {
    }

    public FrontDesktopException(String message){
        super(message);
    }

    public String getFrontDesktopStackTrace() {
        StringBuilder trace = new StringBuilder();
        StackTraceElement[] traceElements = super.getStackTrace();
        Arrays.stream(traceElements).forEach(x-> trace.append(x.toString()+"\n"));
        return trace.toString();
    }
}
