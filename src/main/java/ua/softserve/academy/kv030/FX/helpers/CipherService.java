package ua.softserve.academy.kv030.FX.helpers;

import ua.softserve.academy.kv030.FX.exceptions.CipherException;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static javax.crypto.Cipher.DECRYPT_MODE;
import static ua.softserve.academy.kv030.FX.values.Constants.*;

public class CipherService {

    private static byte[] iv = new byte[16];

    public static byte[] encrypt(byte[] fileBytes, String key) throws CipherException {

        if (key.length() != KEY_SIZE) {
            throw new CipherException("Illegal key size");
        } else {
            try {
                Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
                SecretKey secretKey = new SecretKeySpec(key.getBytes(), KEY_ALGORITHM);
                IvParameterSpec ivSpec = new IvParameterSpec(iv);
                cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
                return cipher.doFinal(fileBytes);
            } catch (InvalidAlgorithmParameterException |
                    InvalidKeyException | BadPaddingException | NoSuchPaddingException |
                    NoSuchAlgorithmException | IllegalBlockSizeException e) {
                throw new CipherException("ENCRYPTION: " + e.getMessage());
            }
        }
    }

    public static byte[] decrypt(byte[] fileBytes, String key) throws CipherException {
        if (key.length() != KEY_SIZE) {
            throw new CipherException("Illegal key size");
        } else {
            try {
                Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
                SecretKey secretKey = new SecretKeySpec(key.getBytes(), KEY_ALGORITHM);
                IvParameterSpec ivSpec = new IvParameterSpec(iv);
                cipher.init(DECRYPT_MODE, secretKey, ivSpec);
                return cipher.doFinal(fileBytes);
            } catch (InvalidAlgorithmParameterException |
                    InvalidKeyException | BadPaddingException | NoSuchPaddingException |
                    NoSuchAlgorithmException | IllegalBlockSizeException e) {
                throw new CipherException("DECRYPTION: " + e.getMessage());
            }
        }
    }

    public static String generateKey(){
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(KEY_ALGORITHM);
            keyGen.init(128);
            SecretKey secretKey = keyGen.generateKey();
            String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
            return encodedKey.substring(0, 16);
        } catch (NoSuchAlgorithmException e) {
            throw new CipherException("KEY GENERATION: " + e.getMessage());
        }
    }
}
