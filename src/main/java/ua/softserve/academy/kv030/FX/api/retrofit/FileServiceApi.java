package ua.softserve.academy.kv030.FX.api.retrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;
import ua.softserve.academy.kv030.FX.model.ResourceDTO;
import ua.softserve.academy.kv030.FX.model.ShareDataDTO;

import java.util.List;

public interface FileServiceApi {

    @Headers({
            "Accept:application/json"
    })
    @Multipart
    @POST("user/files/uploadFile")
    Call<ResourceDTO> uploadFile(
            @Part("ownerId") RequestBody ownerId,
            @Part("expirationTime") RequestBody expirationTime,
            @Part("fileSize") RequestBody fileSize,
            @Part("fileName") RequestBody fileName,
            @Part("mime") RequestBody mime,
            @Part("permission") RequestBody permission,
            @Part("key") RequestBody key,
            @Part MultipartBody.Part file
    );

    @Headers({
            "Desktop: encrypted"
    })
    @GET("files/{fileId}")
    Call<ResponseBody> downloadFile(
            @Path("fileId") String fileId
    );

    @Headers({
            "Accept: application/json"
    })
    @GET("/user/{userId}/files")
    Call<List<ResourceDTO>> getUserFilesList(
            @Path("userId") long userId
    );

    @DELETE("/user/{userId}/files/{fileId}")
    Call<ResponseBody> deleteFile(@Path("userId") long userId, @Path("fileId") long fileId);

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @PUT("/user/files/share")
    Call<ResponseBody> shareFile(@Body ShareDataDTO shareData);

}
