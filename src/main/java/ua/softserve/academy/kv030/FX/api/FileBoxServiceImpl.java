package ua.softserve.academy.kv030.FX.api;

import javafx.application.Platform;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ua.softserve.academy.kv030.FX.api.retrofit.FileServiceApi;
import ua.softserve.academy.kv030.FX.api.retrofit.ServiceGenerator;
import ua.softserve.academy.kv030.FX.exceptions.FrontDesktopException;
import ua.softserve.academy.kv030.FX.model.PermissionDTO;
import ua.softserve.academy.kv030.FX.model.ResourceDTO;
import ua.softserve.academy.kv030.FX.model.ShareDataDTO;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import static okhttp3.MultipartBody.Part.createFormData;
import static okhttp3.RequestBody.create;
import static ua.softserve.academy.kv030.FX.helpers.CipherService.generateKey;
import static ua.softserve.academy.kv030.FX.helpers.FileTools.*;
import static ua.softserve.academy.kv030.FX.helpers.UITools.*;
import static ua.softserve.academy.kv030.FX.values.Constants.*;

public class FileBoxServiceImpl implements FileBoxService {

    private FileServiceApi fileService;

    public FileBoxServiceImpl() {
        this.fileService = ServiceGenerator.createService(FileServiceApi.class, USER_TOKEN, AUTH_URL);
    }

    @Override
    public void uploadFile(File file, LocalDate localDate) {
        ResourceDTO metaData = prepareDTO(file, localDate);
        MultipartBody.Part fileToTransfer = prepareFile(file, metaData);

        Call<ResourceDTO> call = doFileUpload(metaData, fileToTransfer);
        call.enqueue(new Callback<ResourceDTO>() {
            @Override
            public void onResponse(Call<ResourceDTO> call, Response<ResourceDTO> response) {
                if (response.isSuccessful()) {
                    ResourceDTO resourceDTO = response.body();
                    showNotifLink("Link to your file:", resourceDTO.getFileUUID());
                } else showError("Access denied");
            }
            @Override
            public void onFailure(Call<ResourceDTO> call, Throwable throwable) {
                showError("Connection failed");
            }});
    }

    @Override
    public void downloadFile(String link) {
        Call<ResponseBody> call = fileService.downloadFile(link);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        byte[] bytes = response.body().bytes();
                        if (bytes.length <= CIPHER_FILE_SIZE) {
                            bytes = decryptBytes(bytes, fileKeyFromResponse(response));
                        }
                        File file = createFile(bytes, fileNameFromResponse(response));
                        Platform.runLater(() -> openFile(file));
                    } catch (IOException e) {
                        throw new FrontDesktopException(e.getMessage());
                    }
                } else showError("Access denied");
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                showError("Connection Failed");
            }});
    }

    @Override
    public boolean deleteFile(long userId, long fileId) {
        Call<ResponseBody> call = fileService.deleteFile(userId, fileId);
        return execute(call, "File was deleted", "File was not deleted");
    }

    @Override
    public void shareFile(ShareDataDTO shareDataDTO) {
        Call<ResponseBody> call = fileService.shareFile(shareDataDTO);
        execute(call, "File was shared", "File was not shared");
    }

    @Override
    public List<ResourceDTO> getUserFilesList(long userId) {
        Call<List<ResourceDTO>> call = fileService.getUserFilesList(USER_ID);
        try {
            return call.execute().body();
        } catch (Exception e) {
            showNotif("Failed");
            throw new FrontDesktopException(e.getMessage());
        }
    }

    private boolean execute(Call<ResponseBody> call, String success, String fail) {
        try {
            if (call.execute().isSuccessful()) {
                showNotif(success);
                return true;
            } else {
                showNotif(fail);
                return false;
            }
        } catch (IOException e) {
            return false;
        }
    }

    private Call<ResourceDTO> doFileUpload(ResourceDTO fileMetadata, MultipartBody.Part fileToSend) {
        RequestBody ownerId = createSimpleBody(fileMetadata.getOwnerId());
        RequestBody expirationTime = createSimpleBody(fileMetadata.getExpirationTime().toString());
        RequestBody fileSize = createSimpleBody(fileMetadata.getFileSize());
        RequestBody fileName = createSimpleBody(fileMetadata.getFileName());
        RequestBody mime = createSimpleBody(fileMetadata.getMime());
        RequestBody permission = createSimpleBody(fileMetadata.getPermission().getValue());
        RequestBody key = createSimpleBody(fileMetadata.getKey());
        return fileService.uploadFile(ownerId, expirationTime, fileSize, fileName, mime, permission, key, fileToSend);
    }

    private ResourceDTO prepareDTO(File yourFile, LocalDate localDate) {
        OffsetDateTime date = OffsetDateTime.of(localDate, LocalTime.MIDNIGHT, ZoneOffset.UTC);
        ResourceDTO metaData = new ResourceDTO();
        metaData.setResourceId(USER_ID);
        metaData.setOwnerId(USER_ID);
        metaData.setFileUUID(UUID.randomUUID().toString());
        metaData.setExpirationTime(date);
        metaData.setFileSize(yourFile.length());
        metaData.setFileName(yourFile.getName());
        metaData.setMime(yourFile.getName().substring(yourFile.getName().indexOf(".")));
        metaData.setPermission(PermissionDTO.ALL_USERS);
        metaData.setKey(generateKey());
        return metaData;
    }

    private MultipartBody.Part prepareFile(File yourFile, ResourceDTO metaData) {
        String mime;
        try {
            mime = Files.probeContentType(yourFile.toPath());
        } catch (IOException e) {
            throw new FrontDesktopException(e.getMessage());
        }

        if (yourFile.length() <= CIPHER_FILE_SIZE) {
            yourFile = encryptFile(yourFile, metaData.getKey());
        }

        RequestBody filePart = create(MediaType.parse(mime), yourFile);
        return createFormData("file", yourFile.getName(), filePart);
    }

    private RequestBody createSimpleBody(String param) {
        return RequestBody.create(MediaType.parse("text/plain"), param);
    }

    private RequestBody createSimpleBody(long param) {
        return RequestBody.create(MediaType.parse("text/plain"), String.valueOf(param));
    }

}