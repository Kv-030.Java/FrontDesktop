package ua.softserve.academy.kv030.FX.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ua.softserve.academy.kv030.FX.api.FileBoxService;
import ua.softserve.academy.kv030.FX.api.FileBoxServiceImpl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

import static ua.softserve.academy.kv030.FX.values.Constants.USER_TOKEN;

public class UploadFileController {

    @FXML
    AnchorPane uploadPane;
    @FXML
    Button chooseFileButton, uploadFileButton;
    @FXML
    Label fileLabel;
    @FXML
    DatePicker expirationDatePicker;

    private static FileChooser fileChooser = new FileChooser();
    private static Stage stage = new Stage();
    private File yourFile;

    public void chooseFile() {
        fileChooser.setTitle("Document Box");
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        try {
            String path = fileChooser.showOpenDialog(stage).getAbsolutePath();
            yourFile = new File(path);
            fileLabel.setText(yourFile.getName());
        } catch (Exception e) {
            fileLabel.setText("No file is chosen yet");
        }
    }

    public void handleEnterPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER)
            chooseFile();
    }

    public void uploadFile() throws IOException {
        FileBoxService fileService = new FileBoxServiceImpl();
        System.out.println(USER_TOKEN);
        LocalDate date = expirationDatePicker.getValue();
        fileService.uploadFile(yourFile, date);
    }
}
