package ua.softserve.academy.kv030.FX.api.retrofit;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ua.softserve.academy.kv030.FX.model.UserCredentialsDTO;
import ua.softserve.academy.kv030.FX.model.UserDTO;

import java.util.List;

public interface UserServiceApi {

    @POST("user/login/")
    Call<UserDTO> login(@Body UserCredentialsDTO userCredentialsDTO);

    @Headers({
            "Accept:application/json"
    })
    @GET("/users")
    Call<List<UserDTO>> getUsers();
}
