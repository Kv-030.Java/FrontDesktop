package ua.softserve.academy.kv030.FX.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ua.softserve.academy.kv030.FX.helpers.OffsetDateTimeDeserializer;
import ua.softserve.academy.kv030.FX.helpers.OffsetDateTimeSerializer;

import java.time.OffsetDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "resourceId",
        "ownerId",
        "fileUUID",
        "expirationTime",
        "fileSize",
        "fileName",
        "mime",
        "permission",
        "key"
})
public class ResourceDTO {

    @JsonProperty("resourceId")
    private long resourceId;
    @JsonProperty("ownerId")
    private long ownerId;
    @JsonProperty("fileUUID")
    private String fileUUID;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonProperty("expirationTime")
    private OffsetDateTime expirationTime;
    @JsonProperty("fileSize")
    private long fileSize;
    @JsonProperty("fileName")
    private String fileName;
    @JsonProperty("mime")    private String mime;
    @JsonProperty("permission")
    private PermissionDTO permission;
    @JsonProperty("key")
    private String key;


    /**
     * No args constructor for use in serialization
     */
    public ResourceDTO() {
    }

    /**
     * @param fileSize
     * @param resourceId
     * @param expirationTime
     * @param ownerId
     * @param fileName
     * @param permission
     * @param mime
     * @param key
     * @param fileUUID
     */
    public ResourceDTO(long resourceId, long ownerId, String fileUUID, OffsetDateTime expirationTime, long fileSize, String fileName, String mime, PermissionDTO permission, String key) {
        super();
        this.resourceId = resourceId;
        this.ownerId = ownerId;
        this.fileUUID = fileUUID;
        this.expirationTime = expirationTime;
        this.fileSize = fileSize;
        this.fileName = fileName;
        this.mime = mime;
        this.permission = permission;
        this.key = key;
    }

    @JsonProperty("resourceId")
    public long getResourceId() {
        return resourceId;
    }

    @JsonProperty("resourceId")
    public void setResourceId(long resourceId) {
        this.resourceId = resourceId;
    }

    @JsonProperty("ownerId")
    public long getOwnerId() {
        return ownerId;
    }

    @JsonProperty("ownerId")
    public void setOwnerId(long ownerId) {
        this.ownerId = ownerId;
    }

    @JsonProperty("fileUUID")
    public String getFileUUID() {
        return fileUUID;
    }

    @JsonProperty("fileUUID")
    public void setFileUUID(String fileUUID) {
        this.fileUUID = fileUUID;
    }

    @JsonProperty("expirationTime")
    public OffsetDateTime getExpirationTime() {
        return expirationTime;
    }

    @JsonProperty("expirationTime")
    public void setExpirationTime(OffsetDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }

    @JsonProperty("fileSize")
    public long getFileSize() {
        return fileSize;
    }

    @JsonProperty("fileSize")
    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    @JsonProperty("fileName")
    public String getFileName() {
        return fileName;
    }

    @JsonProperty("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @JsonProperty("mime")
    public String getMime() {
        return mime;
    }

    @JsonProperty("mime")
    public void setMime(String mime) {
        this.mime = mime;
    }

    @JsonProperty("permission")
    public PermissionDTO getPermission() {
        return permission;
    }

    @JsonProperty("permission")
    public void setPermission(PermissionDTO permission) {
        this.permission = permission;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }


    public int describeContents() {
        return 0;
    }

    public ResourceDTO withFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public ResourceDTO withFileSize(long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public ResourceDTO withMime(String type) {
        this.mime = type;
        return this;
    }

    @Override
    public String toString() {
        return fileName;
    }
}


